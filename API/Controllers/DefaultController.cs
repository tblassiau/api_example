﻿using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.API.Controllers
{
    public class DefaultController : Controller
    {
        [HttpGet][Route("/testSuccess")]
        public IActionResult testSuccess()
        {
            return Ok("success");
        }
        
        [HttpGet][Route("/testNotFound")]
        public IActionResult testNotFound()
        {
            return NotFound("not found");
        }
        
        [HttpGet][Route("/testError")]
        public string testError()
        {
            return "error";
        }
    }
}