namespace WebApplication1.API.Models
{
    public class Pet
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}