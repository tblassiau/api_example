using Microsoft.EntityFrameworkCore;
using WebApplication1.API.Models;

namespace WebApplication1.API
{
    public class AppDbContext : DbContext
    {
        public DbSet<Pet> Pets { get; set; }
        public DbSet<User> Users { get; set; }
        
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}
    }
}