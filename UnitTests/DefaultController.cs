using Microsoft.AspNetCore.Mvc;
using WebApplication1.API.Controllers;
using Xunit;

namespace WebApplication1.UnitTests
{
    public class UnitTestDefaultController
    {
        [Fact]
        public void TestSuccess()
        {
            var controller = new DefaultController();
            var response = controller.testSuccess() as ObjectResult;
            
            Xunit.Assert.NotNull(response);
            Xunit.Assert.Equal(200, response.StatusCode);
        }
        
        [Fact]
        public void TestNotFound()
        {
            var controller = new DefaultController();
            var response = controller.testNotFound() as ObjectResult;
            
            Xunit.Assert.NotNull(response);
            Xunit.Assert.Equal(404, response.StatusCode);
        }
        
        [Fact]
        public void TestError()
        {
            var controller = new DefaultController();
            var response = controller.testError();

            Xunit.Assert.Equal("error", response);
        }
        
        [Fact]
        public void FailingTest()
        {
            Xunit.Assert.True(false);
        }
    }
}