## Requirements
For local development, bazel will take care of those on the pipeline.

| Name                                  | Version   |
|---------------------------------------|-----------|
| Microsoft.EntityFrameworkCore         | 3.1.10    |
| Microsoft.EntityFrameworkCore.Tools   | 3.1.10    |
| Pomelo.EntityFrameworkCore.MySql      | 3.2.4     |

## Migration
### Setup Database
``dotnet ef migrations add DbInit``
### Update
``dotnet ef database -f``

## Building
``bazel build //...``

## Testing
``bazel test //...``

## Run
``bazel run //api.exe``